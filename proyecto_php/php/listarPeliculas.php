<?php
include("conexion.php");

$consulta = "SELECT * FROM pelicula";

$resultset = $conexion -> query($consulta) or die("No se pueden recibir datos de productos <br>" . $conexion -> error);

?>

<table id="lista_peliculas">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th>Género</th>
    <th>Año</th>
    <th>Valoración</th>
    <th colspan="2">Operaciones</th>
  </tr>
  <?php
    while($registro = $resultset -> fetch_row()){
        echo "<tr>";
        echo "<td>" . $registro[0] . "</td>";
        echo "<td>" . $registro[1] . "</td>";
        echo "<td>" . $registro[2] . "</td>";
        echo "<td>" . $registro[3] . "</td>";
        echo "<td>" . $registro[4] . "</td>";
        echo "<td> <button type='button' onclick='editarPelicula(".$registro[0].");'>";
        echo "Editar</button></td>";
        echo "<td> <button type='button' value='Eliminar' onclick='eliminarPelicula(".$registro[0].");'>";
        echo "Eliminar</button></td>";
        echo "</tr>";
    }
  ?>
</table>