-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-07-2020 a las 05:44:24
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `peliculas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id_pelicula` int(11) NOT NULL,
  `nombre_pelicula` varchar(100) NOT NULL,
  `genero_pelicula` varchar(100) NOT NULL,
  `anio_pelicula` int(4) NOT NULL,
  `valoracion_pelicula` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id_pelicula`, `nombre_pelicula`, `genero_pelicula`, `anio_pelicula`, `valoracion_pelicula`) VALUES
(1, 'Interestelar', 'Ciencia Ficción', 2014, 8),
(2, 'El hombre de acero', 'Ciencia Ficción', 2013, 7),
(3, 'El Hobbit', 'Aventura', 2012, 7),
(4, 'La esposa del viajero del tiempo', 'Romance', 2009, 7),
(5, 'Tango para tres', 'Romance', 1999, 6),
(6, 'John Wick', 'Acción', 2014, 7),
(7, 'El origen', 'Ciencia Ficción', 2010, 8),
(8, 'Transformers', 'Acción', 2007, 7),
(9, 'Hostel', 'Suspenso', 2005, 5),
(10, 'Terror en el desierto', 'Terror', 1990, 7),
(11, 'Esto es guerra', 'Comedia Romántica', 2012, 6),
(12, 'Lazos perversos', 'Drama', 2013, 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id_pelicula`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id_pelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
