function formPelicula(){
	window.location = "php/editarPelicula.php";
}

function editarPelicula(id){
	window.location = "php/editarPelicula.php?id=" + id;
}

function eliminarPelicula(id){
	
	var confirmar = confirm("¿Confirmar eliminación?");

	if(confirmar){
		$.ajax({
			type: 'POST',
			url: 'php/eliminarPelicula.php',
			data: {id: id},
			cache: false,
			success: function(respuesta){
				alert(respuesta);
				location.reload();
			}
		});
	}

}